class GuestSessionsController < ApplicationController
  def create
    user = User.find_by(email: "test@example.com")
    log_in(user)
    flash[:success] = "ゲストユーザーとしてログインしました"
    redirect_to root_path
  end
end
