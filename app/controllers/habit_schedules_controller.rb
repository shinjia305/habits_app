class HabitSchedulesController < ApplicationController
  def done
    @checkbox = Checkbox.all.find(params[:checkbox_id])
    @checkbox.update(checkbox: true)
  end

  def notdone
    @checkbox = Checkbox.all.find(params[:checkbox_id])
    @checkbox.update(checkbox: false)
  end
end
