class HabitsController < ApplicationController
  before_action :logged_in_user

  def index
    @habits = current_user.habits
  end

  def show
    @habit = current_user.habits.find(params[:id])
  end

  def new
    @habit = current_user.habits.new

    case @habit.datatype
    when "checkbox"
      @checkbox = Checkbox.new
    when "number"
      @number = Number.new
    when "evaluation"
      @evaluation = Evaluation.new
    end
  end

  def create
    @habit = current_user.habits.new(habit_params)

    if @habit.save
      case @habit.datatype
      when "checkbox"
        -14.upto(15) do |i|
          @checkbox = Checkbox.create(habit_id: @habit.id, scheduled_date: Date.today + i, user_id: current_user.id)
        end
      when "number"
        -14.upto(15) do |i|
          @number = Number.create(habit_id: @habit.id, scheduled_date: Date.today + i, user_id: current_user.id)
        end
      when "evaluation"
        -14.upto(15) do |i|
          @evaluation = Number.create(habit_id: @habit.id, scheduled_date: Date.today + i, user_id: current_user.id)
        end
      end

      flash[:success] = "習慣を作成しました"
      redirect_to habits_url
    else
      render "new"
    end
  end

  def edit
    @habit = current_user.habits.find(params[:id])
  end

  def update
    @habit = current_user.habits.find(params[:id])
    if @habit.update(habit_params)
      flash[:success] = "習慣を更新しました"
      redirect_to habits_url
    else
      render "edit"
    end
  end

  def destroy
    @habit = current_user.habits.find(params[:id])
    @habit.destroy
    flash[:success] = "習慣を削除しました"
    redirect_to habits_url
  end

  def sort
    habit = Habit.find(params[:habit_id])
    habit.update(habit_params)
    render body: nil
  end

  private

  def habit_params
    params.require(:habit).permit(:name, :icon, :row_order_position, :datatype, active_day: [])
  end
end
