class StaticPagesController < ApplicationController
  def home
    if logged_in?
      @habits = current_user.habits.rank(:row_order).order(:id)
      @habit_schedules = current_user.habit_schedules.order(:id)
      @checkboxes = Checkbox.all.where(user_id: current_user.id).order(:id)
      @numbers = Number.all.where(user_id: current_user.id)
    end
  end

  def about
  end
end
