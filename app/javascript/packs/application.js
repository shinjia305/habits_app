// This file is automatically compiled by Webpack, along with any other files
// present in this directory. You're encouraged to place your actual application logic in
// a relevant structure within app/javascript and only use these pack files to reference
// that code so it'll be compiled.

require("@rails/ujs").start()
require("turbolinks").start()
require("@rails/activestorage").start()
require("channels")
require("jquery")
require("jquery-ui")

$(function() {
    $( ".sortable" ).sortable({
      update: function(e, ui){
        let item = ui.item;
        let item_data = item.data();
        let params = {_method: 'put'};
        params[item_data.modelName] = { row_order_position: item.index() }
        $.ajax({
          type: 'POST',
          url: item_data.updateUrl,
          dataType: 'json',
          data: params
        });
      },
      stop: function(e, ui){
        ui.item.children('td').not('.item__status').effect('highlight', { color: "#FFFFCC" }, 500)
      }
   });
});

// Uncomment to copy all static images under ../images to the output folder and reference
// them with the image_pack_tag helper in views (e.g <%= image_pack_tag 'rails.png' %>)
// or the `imagePath` JavaScript helper below.
//
// const images = require.context('../images', true)
// const imagePath = (name) => images(name, true)

import "bootstrap"
import "../stylesheets/application"
import "@fortawesome/fontawesome-free/js/all";
