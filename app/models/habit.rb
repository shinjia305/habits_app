class Habit < ApplicationRecord
  belongs_to :user
  has_many :habit_schedules, dependent: :destroy
  has_many :habit_active_days_of_the_weeks, dependent: :destroy

  include RankedModel
  ranks :row_order, with_same: :user_id

  validates :user_id,
            presence: true

  validates :name,
            presence: true,
            length: { maximum: 30, minimum: 1 }

  validates :icon,
            presence: true

  validates :datatype,
            presence: true

  validates :active_day,
            presence: true
end
