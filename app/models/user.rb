class User < ApplicationRecord
  has_many :habits, dependent: :destroy
  has_many :habit_schedules, dependent: :destroy
  before_save { self.email = email.downcase }

  validates :name,
            presence: true,
            length: { maximum: 16 }

  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i.freeze # for email
  validates :email,
            presence: true,
            length: { maximum: 30 + 13 },
            format: { with: VALID_EMAIL_REGEX },
            uniqueness: { case_sensitive: false }

  has_secure_password
  validates :password,
            presence: true,
            length: { minimum: 6 },
            allow_nil: true
end
