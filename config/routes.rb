Rails.application.routes.draw do
  root "static_pages#home"
  get  "static_pages/about"
  resources :users, except: [:index]
  resources :sessions, only: [:new, :create, :destroy]
  resources :habits do
    post 'habit_schedules/:checkbox_id/done', to: 'habit_schedules#done', as: :done
    post 'habit_schedules/:checkbox_id/notdone', to: 'habit_schedules#notdone', as: :notdone
    put :sort
  end
  post "/guest", to: "guest_sessions#create"
end
