class AddIconToTasks < ActiveRecord::Migration[6.0]
  def change
    add_column :tasks, :icon, :string
  end
end
