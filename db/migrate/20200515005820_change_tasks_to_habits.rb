class ChangeTasksToHabits < ActiveRecord::Migration[6.0]
  def change
    rename_table :tasks, :habits
  end
end
