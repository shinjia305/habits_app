class AddDatatypeToHabits < ActiveRecord::Migration[6.0]
  def change
    add_column :habits, :datatype, :string
  end
end
