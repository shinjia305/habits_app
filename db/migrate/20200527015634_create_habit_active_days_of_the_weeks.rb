class CreateHabitActiveDaysOfTheWeeks < ActiveRecord::Migration[6.0]
  def change
    create_table :habit_active_days_of_the_weeks do |t|
      t.integer :active_day_of_the_week
      t.references :habit, null: false, foreign_key: true

      t.timestamps
    end
  end
end
