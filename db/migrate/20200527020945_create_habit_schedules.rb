class CreateHabitSchedules < ActiveRecord::Migration[6.0]
  def change
    create_table :habit_schedules do |t|
      t.datetime :scheduled_date
      t.string :type
      t.integer :number
      t.boolean :checkbox
      t.integer :evaluation
      t.references :habit, null: false, foreign_key: true

      t.timestamps
    end
  end
end
