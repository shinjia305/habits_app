class AddUserIdToHabitSchedules < ActiveRecord::Migration[6.0]
  def change
    add_column :habit_schedules, :user_id, :integer
  end
end
