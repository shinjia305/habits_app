class AddActiveDayToHabits < ActiveRecord::Migration[6.0]
  def change
    add_column :habits, :active_day, :text
  end
end
