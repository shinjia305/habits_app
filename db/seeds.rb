# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

User.create!(
  name: "Guest User",
  email: "test@example.com",
  password: "password",
  password_confirmation: "password"
)

Habit.create!(
  [
    user_id: 1,
    name: "ランニング10分",
    icon: "fas fa-running",
    datatype: "checkbox",
    active_day: ["0","1","2","3","4","5","6"]
  ]
)

Checkbox.create!(
  [
    user_id: 1,
    habit_id: 1
  ]
)
