FactoryBot.define do
  factory :habit do
    sequence(:name) { |n| "TEST_HABIT_NAME#{n}" }
    icon            { "FONTAWESOME" }
    datatype        { "checkbox" }
    active_day      { "1" }
  end
end
