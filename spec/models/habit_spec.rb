require 'rails_helper'

RSpec.describe Habit, type: :model do
  before do
    @user = User.create(
      name: "Asai Shinji",
      email: "hogehoge@gmail.com",
      password: "hogehoge",
      password_confirmation: "hogehoge"
    )

    @task = @user.habits.create(
      name: "ランニング",
      user_id: @user.id,
      icon: "fas fa-check-circle",
      datatype: "checkbox",
      active_day: "1"
    )
  end

  it "name、user_id、icon、datatypeが入力されていればタスクは有効な状態であること" do
    expect(@task).to be_valid
  end

  it "習慣タスクの名前がなければ無効な状態であること" do
    @task.name = nil
    expect(@task).not_to be_valid
  end

  it "習慣タスクのuser_idがなければ無効な状態であること" do
    @task.user_id = nil
    expect(@task).not_to be_valid
  end

  it "習慣タスクのアイコンがなければ無効な状態であること" do
    @task.icon = nil
    expect(@task).not_to be_valid
  end

  it "習慣タスクのデータタイプがなければ無効な状態であること" do
    @task.datatype = nil
    expect(@task).not_to be_valid
  end

  it "習慣タスク名が長すぎないこと" do
    @task.name = "a" * 31
    expect(@task).not_to be_valid
  end
end
