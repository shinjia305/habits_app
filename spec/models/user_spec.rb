require 'rails_helper'

RSpec.describe User, type: :model do
  before do
    @user = User.create(
      name: "Asai Shinji",
      email: "hogehoge@gmail.com",
      password: "hogehoge",
      password_confirmation: "hogehoge"
    )
  end

  it "名前、メール、パスワード、パスワード確認が入力されていれば有効な状態であること" do
    expect(@user).to be_valid
  end

  it "名前がなければ無効な状態であること" do
    @user.name = nil
    expect(@user).not_to be_valid
  end

  it "emailがなければ無効な状態であること" do
    @user.email = nil
    expect(@user).not_to be_valid
  end

  it "nameの文字数が長すぎないこと" do
    @user.name = "a" * 17
    expect(@user).not_to be_valid
  end

  it "emailが長すぎないこと" do
    @user.email = "a" * 40 + "@example.com"
    expect(@user).not_to be_valid
  end

  it "有効なemailなら登録可能" do
    valid_addresses = %w(
      user@examle.com USER@foo.COM A_US-ER@foo.bar.org
      first.last@foo.jp alice+bob@buz.cn
    )
    valid_addresses.each do |valid_address|
      @user.email = valid_address
      expect(@user).to be_valid
    end
  end

  it "無効なemailなら登録不能" do
    invalid_addresses = %w(
      user@example,com user_at_foo.org user.name@example.
      foo@bar_baz.com foo@bar+baz.com
    )
    invalid_addresses.each do |invalid_address|
      @user.email = invalid_address
      expect(@user).not_to be_valid
    end
  end

  it "emailは一意であること" do
    duplicate_user = @user.dup
    duplicate_user.email = @user.email.upcase
    duplicate_user.save
    expect(duplicate_user).not_to be_valid
  end

  it "passwordが入力されていること" do
    @user.password = @user.password_confirmation = " " * 6
    expect(@user).not_to be_valid
  end

  it "passwordは最小文字数以上であること" do
    @user.password = @user.password_confirmation = "a" * 5
    expect(@user).not_to be_valid
  end
end
