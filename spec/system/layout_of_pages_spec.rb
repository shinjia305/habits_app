require 'rails_helper'

RSpec.describe "LayoutOfPages", type: :system do
  let(:user)  { create(:user) }
  let(:habit) { create(:habit, user: user) }

  describe "Homeページのレイアウト" do
    context "ログインしていない場合" do
      it "ログインページのタイトルが正常に表示されている" do
        visit root_path
        expect(page).to have_title "Home | 習慣管理アプリ"
      end

      it "ログインページへのリンクが表示されている" do
        visit root_path
        expect(page).to have_link "ログイン"
      end
    end

    context "ログインしている場合" do
      before do
        sign_in_as(user)
      end

      it "Homeページのタイトルが正常に表示されている" do
        visit root_path
        expect(page).to have_title "Home | 習慣管理アプリ"
      end

      it "Homeページのトップページのリンクが正常に表示されている" do
        visit root_path
        expect(page).to have_link "習慣管理アプリ"
      end
    end
  end

  describe "使い方のページのレイアウト" do
    it "使い方ページへのリンクから使い方のページが表示される" do
      visit root_path
      click_on "使い方"
      expect(page).to have_content "習慣管理アプリの使い方"
    end

    it "使い方ページのタイトルが正常に表示される" do
      visit static_pages_about_path
      expect(page).to have_title "使い方"
    end
  end

  describe "習慣の設定ページのレイアウト" do
    before do
      sign_in_as(user)
    end

    it "習慣設定ページのタイトルが正常に表示されている" do
      visit habits_path
      expect(page).to have_title "習慣の設定 | 習慣管理アプリ"
    end

    it "Homeページのヘッダーからリンクをクリックして習慣設定ページへ正常に移動できる" do
      visit root_path
      first(:css, ".habit-path-link").click
      expect(page).to have_content "習慣の設定"
      expect(page).to have_link "新しく習慣を作成する"
    end
  end

  describe "習慣タスク編集ページのレイアウト" do
    before do
      sign_in_as(user)
    end

    it "習慣タスク編集ページのタイトルが正常に表示されている" do
      visit edit_habit_path(habit.id)
      expect(page).to have_title "習慣タスク編集 | 習慣管理アプリ"
    end

    it "習慣設定ページの習慣名をクリックして習慣タスク編集ページへ正常に移動できる" do
      # visit habits_path
      # click_on "#{habit.name}"
      # expect(page).to have_content "習慣タスク編集"
    end
  end

  describe "ユーザーページのレイアウト" do
    before do
      sign_in_as(user)
    end

    it "ユーザーページのタイトルが正常に表示されている" do
      visit user_path(user.id)
      expect(page).to have_title "#{user.name} | 習慣管理アプリ"
    end

    it "Homeページのヘッダーからリンクをクリックしてユーザーページへ正常に移動できる" do
      visit root_path
      first(:css, ".user-path-link").click
      expect(page).to have_content "#{user.name}"
      expect(page).to have_link "プロフィール編集"
    end
  end

  describe "ユーザー編集ページのレイアウト" do
    before do
      sign_in_as(user)
    end

    it "ユーザー編集ページのタイトルが正常に表示されている" do
      visit edit_user_path(user.id)
      expect(page).to have_title "プロフィール編集 | 習慣管理アプリ"
    end

    it "ユーザーページのからプロフィール編集リンクをクリックしてユーザー編集ページへ正常に移動できる" do
      visit user_path(user.id)
      click_on "プロフィール編集"
      expect(page).to have_content "プロフィール編集"
    end
  end
end
